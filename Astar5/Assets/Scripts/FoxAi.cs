﻿using Pathfinding;
using UnityEngine;

public enum StateE{
    movingLeft,
    movingRight,
    fallDown,
}

public enum StateEnemyLadder{
    ladderUp,
    ladderDown
}

public class FoxAi : MonoBehaviour{
    [SerializeField] private Transform targetPosition;
    [SerializeField] private Transform GroundPointChek;
    [SerializeField] private Transform ChekLadderUp;
    [SerializeField] private Transform ChekLadderDown;

    private Seeker seeker;

    public Path path;

    public float speed = 2;

    public float nextWaypointDistance = 0.5f;

    private int currentWaypoint = 0;

    public  float repathRate = 0.5f;
    private float lastRepath = float.NegativeInfinity;

    public bool reachedEndOfPath;

    private Rigidbody2D rb;

    private StateE currientStateEnemy;

    private Vector2 dir;
    private Vector2 enemyPos;
    private Vector2 pointNode;
    private float   distance;
    private bool    _disableMoveX   = true;
    private bool    _disableAllMove = true;
    private bool    _enableLadderUp;
    private bool    _enableLadderDown;

    private float _futureTime;

    public void Start() {
        seeker = GetComponent<Seeker>();

        rb = GetComponent<Rigidbody2D>();
    }

    public void OnPathComplete(Path p) {
        p.Claim(this);
        if(!p.error) {
            if(path != null) path.Release(this);
            path            = p;
            currentWaypoint = 0;
        }
        else {
            p.Release(this);
        }
    }


    private void FixedUpdate() {
        if(_enableLadderUp) {
            MovingLadderUp();
            Debug.Log("сработал апдйт лезть по лестнице вверх ");
        }
        else if(_enableLadderDown) {
            Debug.Log("сработал апдйт лезть по лестнице вниз ");
            MovingLadderDown();
        }

        if(!_disableAllMove) {
            return;
        }

        FindNodeLeft();
        FindNodeRight();
        if(_futureTime < Time.time) {
            
        EnemyNoPath();
        }

        switch(currientStateEnemy) {
            case StateE.movingLeft :
                Debug.Log("иду в лево");
                MoveLeft();
                break;
            case StateE.movingRight :
                Debug.Log("иду в право");
                ;
                MoveRight();
                break;
            case StateE.fallDown :
                Debug.Log("падаю");
                transform.Translate(Vector3.down * speed * Time.deltaTime);
                break;
        }
    }

    public void Update() {
        distance = Vector2.Distance(pointNode, transform.position);

        if(Time.time > lastRepath + repathRate && seeker.IsDone()) {
            lastRepath = Time.time;

            seeker.StartPath(transform.position, targetPosition.position, OnPathComplete);
        }

        if(path == null) {
            return;
        }

        reachedEndOfPath = false;
        float distanceToWaypoint;
        while(true) {
            distanceToWaypoint = Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]);
            if(distanceToWaypoint < nextWaypointDistance) {
                if(currentWaypoint + 1 < path.vectorPath.Count) {
                    currentWaypoint++;
                }
                else {
                    reachedEndOfPath = true;
                    break;
                }
            }
            else {
                break;
            }
        }

        dir       = (path.vectorPath[currentWaypoint] - transform.position).normalized;
        enemyPos  = transform.position;
        pointNode = path.vectorPath[currentWaypoint];
    }

    public void MoveLeft() {
        rb.gravityScale = 1;
        // rb.velocity = Vector2.left * speed;
        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }

    public void MoveRight() {
        // rb.velocity = Vector2.right * speed;
        rb.gravityScale = 1;
        transform.Translate(Vector3.right * speed * Time.deltaTime);
    }

    public void MovingLadderUp() {
        rb.gravityScale = 0;
        // rb.velocity     = Vector2.up;
        transform.Translate(Vector3.up * speed * Time.deltaTime);
    }

    public void MovingLadderDown() {
        rb.gravityScale = 0;
        // rb.velocity     = Vector2.up;
        transform.Translate(Vector3.down * speed * Time.deltaTime);
    }

    public void FindNodeLeft() {
        if(pointNode.x < enemyPos.x && IsGround() && _disableMoveX) {
            currientStateEnemy = StateE.movingLeft;
        }
    }

    public void FindNodeRight() {
        if(pointNode.x > enemyPos.x && IsGround() && _disableMoveX) {
            currientStateEnemy = StateE.movingRight;
        }
    }

    public void EnemyNoPath() {
        if(IsGround() == false) {
            currientStateEnemy = StateE.fallDown;
        }
    }

    private bool IsGround() {
        Vector2      point = GroundPointChek.position;
        RaycastHit2D hit   = Physics2D.Raycast(point, Vector2.down, 0.2f);
        // Debug.Log("что словил рей каст  " + hit.collider);
        if(hit) {
            return hit.collider.CompareTag("Ground");
        }

        return false;
    }

    private bool IsLadderUp() {
        return true;
    }

    private bool IsLadderDown() {
        return true;
    }


    private void OnTriggerStay2D(Collider2D other) {
        Debug.Log("вошел в колайдер");
        if(other.CompareTag("ladder") && enemyPos.y +1 < targetPosition.position.y && _futureTime < Time.time
            //&& enemyPos.y < pointNode.y + 1 // && _futureTime < Time.time
           ) 
        {
            Debug.Log("наш колайдер лестница вверх");
            _disableAllMove    = false;
            transform.position = new Vector2(other.gameObject.transform.position.x, transform.position.y);
            _enableLadderUp    = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("ladder")) {
            _futureTime = Time.time + 1f;

            Debug.Log("вышел с колайдера лестница");

            Transform child = other.gameObject.transform.GetChild(0);
            transform.position = child.position;

            // transform.Translate(new Vector2(child.position.x, child.position.y) * Time.deltaTime);
           
            _enableLadderUp = false;
            _disableAllMove = true;
                
            
            // transform.position = new Vector2(other.gameObject.transform.GetChild(0));

            // rb.velocity = Vector2.zero;
            // transform.position = new Vector2(targetPosition.position.x, targetPosition.position.y);
        }
    }
}
