﻿using UnityEngine;

public class MoveAI : MonoBehaviour{
    private AstarAI _astarAi;

    private Vector2     _dir;
    private Vector2     _velocity;
    private Vector2     _velocityCorrect;
    private Rigidbody2D rb;
    private bool        isDown;
    private float       _futureTime;


    // Start is called before the first frame update
    void Start() {
        _astarAi = GetComponent<AstarAI>();
        rb       = GetComponent<Rigidbody2D>();
    }

    public void Update() {
        var test1 = Mathf.RoundToInt(0.51f);
        var test2 = Mathf.RoundToInt(0.3f);
        
        var test3 = Mathf.RoundToInt(-0.3f);
        var test4 = Mathf.RoundToInt(-0.5f);
        
 
        Debug.Log(test1 + "  test 1  ");
        Debug.Log(test2 + "  test 2  ");
        Debug.Log(test3 + "  test 2  ");
        Debug.Log(test4 + "  test 2  ");
       
    }

    void FixedUpdate() {
        _velocity = _astarAi.GetVelocity();
        // transform.position += _velocity * Time.deltaTime;
        rb.velocity = _velocity;
        _dir        = _astarAi.GetDirection();

        if(Input.GetKey(KeyCode.Space)) {
            Debug.Log("направление  " + _dir);
        }
    }


    private void OnTriggerEnter2D(Collider2D other) {
        Debug.Log("вошел в колайдер   " + other.gameObject.transform.name);
        if(other.CompareTag("ladder") && _futureTime < Time.time) {
            Debug.Log("вошел в колайдер лестница");
            Vector2 ladderPos = new Vector2(other.gameObject.transform.position.x, transform.position.y);
            transform.position = ladderPos;
            rb.gravityScale    = 0f;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if(other.CompareTag("ladder")) {
            _futureTime = Time.time + 0.5f;
            Debug.Log("вышел с  колайдера лестница");
            rb.gravityScale = 33f;
        }
    }

    public void VelocityCorrect() {
        if(_dir.x > _dir.y) {
            _velocityCorrect = new Vector2(-1, 0); //x -0.8 y 0.2
            Debug.Log("лево");
        }

        if(_dir.x < _dir.y) {
            _velocityCorrect = new Vector2(1, 0); // x -0.7 y 0.3
            Debug.Log("право");
        }

        if(_dir.x < _dir.y) {
            _velocityCorrect = new Vector2(0, 1); // x -0.2 y 0.8
          
            Debug.Log("верх");
        }

        if(_dir.x < _dir.y) {
            _velocityCorrect = new Vector2(0, -1);  //x -0.1 y 0.3
            Debug.Log("низ");
        }
    }
}
